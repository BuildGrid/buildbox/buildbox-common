Committers list
===============

This table details the names and GitLab handles of all the people
currently with commit access in BuildBox.

``Last updated: 2020-07-15``

=================== ==============
Name                GitLab handle
=================== ==============
Adam Coldrick       @SotK
Arber Xhindoli      @arberx
Beth White          @bethwhite
Ed Baunton          @edbaunton
Frank Kolarek       @fkolarek
Jeremiah Bonney     @jbonney
Juerg Billeter      @juergbi
Marios Hadjimichael @mhadjimichael
Richard Kennedy     @RichKen
Rohit Kothur        @rkothur
Santiago Gil        @santigl
=================== ==============
