.. _runners:

Runners
=======

**Runners** are in charge of preparing an environment and executing an ``Action`` in it.

Different implementations are free to choose their own way of creating that environment, and may choose to provide features such as sandboxing.

Since different types of runners will have similar functionality, :ref:`buildbox-common` provides a ``Runner`` base class that can be used to reduce code duplication.

Current implementations
-----------------------
buildbox-run-hosttools
^^^^^^^^^^^^^^^^^^^^^^

https://gitlab.com/BuildGrid/buildbox/buildbox/-/tree/master/run-hosttools/

This is the simplest runner, providing no sandboxing. It invokes the build tools that are available on the local system.

buildbox-run-userchroot
^^^^^^^^^^^^^^^^^^^^^^^

https://gitlab.com/BuildGrid/buildbox/buildbox/-/tree/master/run-userchroot/

It allows to execute commands in a sandbox created by `userchroot <https://gitlab.com/BuildGrid/buildbox/userchroot>`_.


buildbox-run-bubblewrap
^^^^^^^^^^^^^^^^^^^^^^^

https://gitlab.com/BuildGrid/buildbox/buildbox/-/tree/master/run-bubblewrap/

It uses `bubblewrap <https://github.com/containers/bubblewrap>`_ to provide sandboxing.

Execution metrics metadata
--------------------------
Runners based on :ref:`buildboxcommon::Runner <buildbox-common-dev>` support returning execution metrics as part of the ``ExecutedActionMetadata``.

See this section for more details: `ExecutedActionMetadata <runner-dev.html#auxiliary-metadata>`_.
