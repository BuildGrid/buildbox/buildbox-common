recc
====

``recc`` is a compiler command launcher that utilizes
the `Remote Execution APIs <https://github.com/bazelbuild/remote-apis>`_ for
caching and remote execution of both compilation and link actions.

When invoked with a C/C++ compilation command, it communicates with
a Remote Execution service (such as `BuildGrid <http://buildgrid.build/>`_)
to first determine whether the same build has been previously completed.
If a cached build result exists, ``recc`` downloads it. Otherwise, it executes the
command locally or enqueues the build to be executed remotely using the 
execution service, which then dispatches the job to a worker, such as
`buildbox-worker <https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/worker/>`_.

A technical introduction to ``recc`` and ``BuildGrid`` can be found in this Bazelcon 2018
`presentation <https://www.youtube.com/watch?v=w1ZA4Rrf91I>`__ , and on this `blog
post <https://www.codethink.com/articles/2018/introducing-buildgrid/>`_.
``recc`` is in active development and in use at `Bloomberg <https://www.techatbloomberg.com/>`__.

For information regarding contributing to this project, please read the
`Contribution guide <https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/recc/CONTRIBUTING.md>`_.

Contact us via the buildteamworld Slack `#recc <https://buildteamworld.slack.com/channels/recc>`_ on Slack.
