#include <gtest/gtest.h>

#include <google/devtools/remoteworkers/v1test2/bots_mock.grpc.pb.h>

#include <buildboxcommon_digestgenerator.h>
#include <buildboxcommon_grpctestserver.h>
#include <buildboxcommon_protos.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_testingutils.h>
#include <buildboxworker_expiretime.h>
#include <buildboxworker_metricnames.h>
#include <buildboxworker_worker.h>

#include <algorithm>
#include <signal.h>
#include <system_error>

namespace proto {
using namespace google::bytestream;
using namespace google::devtools::remoteworkers::v1test2;
using namespace build::bazel::remote::execution::v2;
} // namespace proto

using buildboxcommon::buildboxcommonmetrics::collectedByName;
using buildboxcommon::buildboxcommonmetrics::DurationMetricValue;
using buildboxworker::Worker;
using google::devtools::remoteworkers::v1test2::MockBotsStub;

using namespace testing;

namespace {
const auto digestFunctionInitializer = []() {
    buildboxcommon::DigestGenerator::init();
    return 0;
}();
}

class WorkerTestFixture : public Worker, public ::testing::Test {
  public:
    WorkerTestFixture()
    {
        d_logLevel = "debug";
        d_maxWaitTime = std::chrono::minutes(10);
        // Set path to test runner
        d_runnerCommand = getenv("BUILDBOX_RUN");

        // Ignore SIGPIPE in case of using sockets + grpc without MSG_NOSIGNAL
        // support configured
        struct sigaction sa;
        sa.sa_handler = SIG_IGN;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        if (sigaction(SIGPIPE, &sa, nullptr) < 0) {
            throw std::system_error(errno, std::system_category(),
                                    "Unable to ignore SIGPIPE");
        }
    }
    void addFakeJob(const std::string jobString)
    {
        d_activeJobs.insert(jobString);
    }
    void upsertSingleLease(const proto::Lease &lease)
    {
        std::lock_guard<std::mutex> lock(d_sessionMutex);
        d_maxConcurrentJobs = 1;
        d_stopAfterJobs = 1;
        if (d_session.leases_size() == 0) {
            d_session.add_leases()->CopyFrom(lease);
        }
        else {
            *d_session.mutable_leases(0) = lease;
        }
    }
    void runSingleLease(const proto::Lease &lease,
                        proto::Lease *completedLease, int processTimes = 0)
    {

        upsertSingleLease(lease);
        if (processTimes == 0) {
            initLocalExecutionClient();
            runWorkerWithoutBotSession();
        }
        else {
            bool skipPollDelay = true;
            for (int i = 0; i < processTimes; i++) {
                processLeases(&skipPollDelay);
            }
        }
        {
            std::lock_guard<std::mutex> lock(d_sessionMutex);
            EXPECT_EQ(d_session.leases_size(), 1);
            completedLease->CopyFrom(d_session.leases(0));
        }
    }

    void setBotStatus(const proto::BotStatus newStatus)
    {
        d_botStatus = newStatus;
    }

    google::rpc::Status runAction(proto::Action *action,
                                  const proto::Command &command,
                                  proto::ActionResult *actionResult,
                                  bool cancel = false)
    {
        const auto commandDigest =
            buildboxcommon::DigestGenerator::hash(command);
        action->mutable_command_digest()->CopyFrom(commandDigest);

        buildboxcommon::GrpcTestServer testServer;
        std::thread serverHandler([&]() {
            buildboxcommon::GrpcTestServerContext ctxCap(
                &testServer, "/build.bazel.remote.execution.v2.Capabilities/"
                             "GetCapabilities");
            ctxCap.finish(grpc::Status(grpc::UNIMPLEMENTED, "Unimplemented"));

            proto::ReadRequest expectedReadRequest;
            proto::ReadResponse readResponse;
            expectedReadRequest.set_resource_name(
                "blobs/" + commandDigest.hash() + "/" +
                std::to_string(commandDigest.size_bytes()));
            readResponse.set_data(command.SerializeAsString());
            buildboxcommon::GrpcTestServerContext ctx(
                &testServer, "/google.bytestream.ByteStream/Read");
            ctx.read(expectedReadRequest);
            ctx.writeAndFinish(readResponse);
        });

        this->d_casServer.setUrl(testServer.url());
        this->d_extraRunArgs.push_back("--no-logs-capture");

        this->initLocalExecutionClient();

        proto::Lease updatedLease;

        proto::Lease lease;
        lease.set_id("test-lease");
        lease.mutable_payload()->PackFrom(*action);
        lease.set_state(proto::LeaseState::PENDING);

        try {
            if (cancel) {
                // 2 passes of processing, first: pending -> waiting ack,
                // second: waiting ack -> scheduled
                this->runSingleLease(lease, &updatedLease, 2);
                lease.set_state(proto::LeaseState::CANCELLED);
                // Sleep until the subprocess is created
                // TODO: refactor the code so a real subprocess doesn't have to
                // be created when testing the worker class e.g. create a new
                // abstraction layer to manage the subprocesses instead of
                // using a plain detached thread and use the mock in unit tests
                std::this_thread::sleep_for(std::chrono::seconds(2));
                this->runSingleLease(lease, &updatedLease, 1);
                EXPECT_EQ(updatedLease.state(), proto::LeaseState::CANCELLED);
                EXPECT_EQ(d_activeJobsToOperations.size(), 0);
            }
            else {
                this->runSingleLease(lease, &updatedLease);
                EXPECT_EQ(updatedLease.state(), proto::LeaseState::COMPLETED);
                EXPECT_TRUE(updatedLease.result().UnpackTo(actionResult));
            }
        }
        catch (...) {
            serverHandler.join();
            throw;
        }
        serverHandler.join();

        return updatedLease.status();
    }
};

// Test that even if a long deadline is set with no work given
// the worker returned a short wait time
TEST_F(WorkerTestFixture, WaitTimeNoJobs)
{
    const auto currentTime = std::chrono::system_clock::now();
    const auto excessive_wait_time =
        std::chrono::duration_cast<std::chrono::seconds>(2 *
                                                         this->d_maxWaitTime);

    const auto expectedWaitTime = currentTime + this->s_defaultWaitTime;

    this->d_session.mutable_expire_time()->set_seconds(
        std::chrono::system_clock::to_time_t(currentTime) +
        excessive_wait_time.count());

    ASSERT_TRUE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(currentTime), expectedWaitTime);
}

// If the bot is in a non-ok status, wait time should be respected
// even if the bot has no work.
TEST_F(WorkerTestFixture, WaitTimeNoJobsUnhealthyBot)
{
    // Very specifically calculate the expire time
    // to avoid rounding errors
    const auto currentTime =
        std::chrono::time_point_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now());
    auto deadline = currentTime + this->d_maxWaitTime;
    const auto deadlineSeconds =
        std::chrono::duration_cast<std::chrono::seconds>(
            deadline.time_since_epoch());
    deadline -= deadlineSeconds;
    const auto deadlineRemainder =
        std::chrono::duration_cast<std::chrono::microseconds>(
            deadline.time_since_epoch());

    // Set the expire time in the seconds/nanos format protobuf expects
    this->d_session.mutable_expire_time()->set_seconds(
        deadlineSeconds.count());
    this->d_session.mutable_expire_time()->set_nanos(
        deadlineRemainder.count() * 1e3);

    // Calculate the expected reduction in duration
    const auto factor = static_cast<int64_t>(
        this->d_maxWaitTime.count() *
        buildboxworker::ExpireTime::updateTimeoutPaddingFactor());

    const auto expectedExpireTime =
        currentTime +
        (this->d_maxWaitTime - std::chrono::microseconds(factor));

    // Set the bot as Unhealthy to make it respect the given expire_time
    // even when there's no assigned work
    setBotStatus(proto::BotStatus::UNHEALTHY);
    ASSERT_TRUE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(currentTime), expectedExpireTime);
}

TEST_F(WorkerTestFixture, DefaultWaitTime)
{
    this->addFakeJob("testjob");
    const auto currentTime = std::chrono::system_clock::now();
    const auto expectedWaitTime = currentTime + this->s_defaultWaitTime;

    ASSERT_FALSE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(currentTime), expectedWaitTime);
}

TEST_F(WorkerTestFixture, WaitTimeSaturates)
{
    this->addFakeJob("testjob");
    const auto currentTime = std::chrono::system_clock::now();
    const auto excessive_wait_time =
        std::chrono::duration_cast<std::chrono::seconds>(2 *
                                                         this->d_maxWaitTime);
    const auto expectedWaitTime = currentTime + this->d_maxWaitTime;

    this->d_session.mutable_expire_time()->set_seconds(
        std::chrono::system_clock::to_time_t(currentTime) +
        excessive_wait_time.count());

    ASSERT_TRUE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(currentTime), expectedWaitTime);
}

TEST_F(WorkerTestFixture, WaitTimePercentage)
{
    this->addFakeJob("testjob");
    // Use a starting time with microsecond precision to prevent rounding
    // errors in test as we use `microseconds` throughout
    const auto currentTime =
        std::chrono::time_point_cast<std::chrono::microseconds>(
            std::chrono::system_clock::now());

    // Calculate the deadline in the protobuf format (seconds / nanosecond
    // remainder)
    auto deadline = currentTime + this->d_maxWaitTime;
    const auto deadlineSeconds =
        std::chrono::duration_cast<std::chrono::seconds>(
            deadline.time_since_epoch());
    deadline -= deadlineSeconds;
    const auto deadlineRemainder =
        std::chrono::duration_cast<std::chrono::microseconds>(
            deadline.time_since_epoch());

    this->d_session.mutable_expire_time()->set_seconds(
        deadlineSeconds.count());
    this->d_session.mutable_expire_time()->set_nanos(
        deadlineRemainder.count() * 1e3);

    // Calculate the expected reduction in duration
    const auto factor = static_cast<int64_t>(
        this->d_maxWaitTime.count() *
        buildboxworker::ExpireTime::updateTimeoutPaddingFactor());

    const auto expectedExpireTime =
        currentTime +
        (this->d_maxWaitTime - std::chrono::microseconds(factor));

    ASSERT_TRUE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(currentTime), expectedExpireTime);
}

proto::Command createSleepCommand(int time)
{
    proto::Command command;
    command.add_arguments("/usr/bin/env");
    command.add_arguments("sleep");
    command.add_arguments(std::to_string(time));
    return command;
}

TEST_F(WorkerTestFixture, RunSingleLease)
{
    proto::Action action;
    proto::Command command = createSleepCommand(1);

    proto::ActionResult actionResult;
    const auto status = runAction(&action, command, &actionResult);

    EXPECT_EQ(status.code(), grpc::StatusCode::OK);
    EXPECT_EQ(actionResult.exit_code(), 0);

    ASSERT_TRUE(collectedByName<DurationMetricValue>(
        buildboxworker::MetricNames::TIMER_NAME_EXECUTE_ACTION));
}

TEST_F(WorkerTestFixture, RunSingleLeaseTimeout)
{
    proto::Action action;
    action.mutable_timeout()->CopyFrom(
        google::protobuf::util::TimeUtil::SecondsToDuration(1));
    proto::Command command = createSleepCommand(5);

    proto::ActionResult actionResult;
    const auto status = runAction(&action, command, &actionResult);

    EXPECT_EQ(status.code(), grpc::StatusCode::DEADLINE_EXCEEDED);
    EXPECT_EQ(actionResult.exit_code(), 128 + SIGKILL);
}

TEST_F(WorkerTestFixture, RunLeaseThenCancel)
{
    proto::Action action;
    // long sleep, should be cancelled
    proto::Command command = createSleepCommand(100);

    proto::ActionResult actionResult;
    const auto status = runAction(&action, command, &actionResult, true);

    EXPECT_EQ(status.code(), grpc::StatusCode::CANCELLED);
    EXPECT_EQ(actionResult.exit_code(), 0);
}
