#include <gtest/gtest.h>

#include <buildboxcommon_logging.h>
#include <buildboxworker_expiretime.h>

#include <google/protobuf/timestamp.pb.h>

using namespace buildboxworker;

using namespace testing;

TEST(ExpireTime, convertToTimePoint)
{
    std::chrono::system_clock::time_point now =
        std::chrono::system_clock::now();

    auto seconds = std::chrono::duration_cast<std::chrono::seconds>(
                       now.time_since_epoch())
                       .count();
    auto micros = std::chrono::duration_cast<std::chrono::microseconds>(
                      now.time_since_epoch())
                      .count() -
                  (seconds * 1e6);

    google::protobuf::Timestamp timestamp;
    timestamp.set_seconds(seconds);
    timestamp.set_nanos(micros * 1e3);
    const std::chrono::system_clock::time_point result =
        ExpireTime::convertToTimePoint(timestamp);

    EXPECT_EQ(std::chrono::duration_cast<std::chrono::microseconds>(
                  now.time_since_epoch()),
              std::chrono::duration_cast<std::chrono::microseconds>(
                  result.time_since_epoch()));
}
