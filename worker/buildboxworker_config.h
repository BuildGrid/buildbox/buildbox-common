/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXWORKER_CONFIG
#define INCLUDED_BUILDBOXWORKER_CONFIG

#include <google/devtools/remoteworkers/v1test2/bots.grpc.pb.h>

#include <string>

namespace buildboxworker {

namespace proto {
using namespace google::devtools::remoteworkers::v1test2;
} // namespace proto

struct Config {
    /**
     * Read bot status from 'configFileName'
     * Returns: bot status from 'configFileName' or BotStatus::OK if any error
     *          is detected while trying to read/parse this config file
     */
    static proto::BotStatus
    getStatusFromConfigFile(const std::string &configFileName);

    static proto::BotStatus getStatusFromConfigStream(std::istream *stream);
};

} // namespace buildboxworker

#endif
