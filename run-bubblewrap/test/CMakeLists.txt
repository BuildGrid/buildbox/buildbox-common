include(${CMAKE_SOURCE_DIR}/cmake/BuildboxGTestSetup.cmake)

add_executable(run_bubblewrap_tests
  bubblewrap.t.cpp
  buildboxrunbwrap_tests.m.cpp
  ../buildboxrun_bubblewrap.cpp
)
target_precompile_headers(run_bubblewrap_tests REUSE_FROM common)

target_include_directories(run_bubblewrap_tests PRIVATE "..")
target_link_libraries(run_bubblewrap_tests common ${GTEST_TARGET})
add_test(NAME run_bubblewrap_tests COMMAND run_bubblewrap_tests WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
