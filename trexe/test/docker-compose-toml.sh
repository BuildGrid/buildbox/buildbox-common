#!/bin/bash

set -eu
# wait for buildgrid
sleep 1

mkdir -p /tmp/upload/input/src

echo "foo" >/tmp/upload/input/src/foo.txt

echo "#!/bin/bash
cat foo.txt
cat mapped/foo.txt
echo \$BAR
" >/tmp/upload/input/test.sh

chmod +x /tmp/upload/input/test.sh

# Submit an async job using config file
cat test/test.toml
operation_id=$(trexe --config-file="test/test.toml")
sleep 5
# Fetch the result
trexe --config-file="test/test.toml" --operation "$operation_id"

out=$(</tmp/stdout)
expected=$(printf "foo\nfoo\nbar")

if [[ "$out" != "$expected" ]]; then
    diff <(echo $out) <(echo "$expected")
    exit 1
fi
