/*
 * Copyright 2022 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INDLUDED_TREXE_CMDLINESPEC
#define INDLUDED_TREXE_CMDLINESPEC

#include <vector>

#include <buildboxcommon_commandlinetypes.h>
#include <buildboxcommon_connectionoptions_commandline.h>

namespace trexe {

struct CmdLineSpec {
    CmdLineSpec(
        buildboxcommon::ConnectionOptionsCommandLine const &spec,
        buildboxcommon::ConnectionOptionsCommandLine const &casSpec,
        buildboxcommon::ConnectionOptionsCommandLine const &acSpec,
        buildboxcommon::ConnectionOptionsCommandLine const &execSpec,
        buildboxcommon::ConnectionOptionsCommandLine const &logstreamSpec);

    std::vector<buildboxcommon::CommandLineTypes::ArgumentSpec> d_spec;

    // positionals
    std::vector<std::string> d_command;
};

} // namespace trexe

#endif
