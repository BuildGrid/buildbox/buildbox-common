#include "buildboxcasd_proxymodefixture.h"
#include <buildboxcasd_localcasservice.h>

INSTANTIATE_TEST_SUITE_P(
    DynamicInstance, CasProxyFixture,
    testing::Combine(testing::Values(DYNAMIC_INSTANCE),
                     testing::Values(FINDMISSINGBLOBS_CACHE_OFF,
                                     FINDMISSINGBLOBS_CACHE_ON),
                     testing::ValuesIn(TEST_INSTANCE_NAMES),
                     testing::Values(NON_THREADED, THREADED)));

GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(
    CasProxyUnreachableRemoteFixture);
GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(RemoteServerFixture);
