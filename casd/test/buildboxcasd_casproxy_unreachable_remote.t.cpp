#include <buildboxcasd_casproxy.h>

INSTANTIATE_TEST_SUITE_P(
    UnReachableRemote, CasProxyUnReachableRemoteFixture,
    testing::Combine(testing::Values(STATIC_INSTANCE, DYNAMIC_INSTANCE),
                     testing::Values(FINDMISSINGBLOBS_CACHE_OFF,
                                     FINDMISSINGBLOBS_CACHE_ON),
                     testing::ValuesIn(TEST_INSTANCE_NAMES),
                     testing::Values(NON_THREADED)));

GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(CasProxyFixture);
GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(
    CasProxyWithFindMissingBlobsCacheFixture);
