/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_fetchtreecache.h>

namespace buildboxcasd {

const unsigned int FetchTreeCache::s_root_digest_ttl_seconds = 3 * 60;

FetchTreeCache::FetchTreeCache()
    : d_trees(s_root_digest_ttl_seconds),
      d_trees_with_files(s_root_digest_ttl_seconds)
{
}

void FetchTreeCache::addRootDigest(const buildboxcommon::Digest &digest,
                                   const bool with_files)
{
    d_trees.addDigest(digest);

    if (with_files) {
        d_trees_with_files.addDigest(digest);
    }
}

bool FetchTreeCache::hasRootDigest(const buildboxcommon::Digest &digest,
                                   const bool with_files)
{
    if (with_files) {
        return d_trees_with_files.hasDigest(digest);
    }
    return d_trees.hasDigest(digest);
}

} // namespace buildboxcasd
