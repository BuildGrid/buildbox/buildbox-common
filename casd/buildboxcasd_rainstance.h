/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_RAINSTANCE_H
#define INCLUDED_BUILDBOXCASD_RAINSTANCE_H

#include <buildboxcommon_protos.h>

using namespace build::bazel::remote::asset::v1;

namespace buildboxcasd {

class RaInstance // NOLINT (cppcoreguidelines-special-member-functions)
{
    /* Defines a common interface for providing the necessary methods to
     * service the calls of the Remote Asset API.
     */
  public:
    RaInstance();
    virtual ~RaInstance() = 0;

    virtual grpc::Status FetchBlob(const FetchBlobRequest &request,
                                   FetchBlobResponse *response) = 0;

    virtual grpc::Status FetchDirectory(const FetchDirectoryRequest &request,
                                        FetchDirectoryResponse *response) = 0;

    virtual grpc::Status PushBlob(const PushBlobRequest &request,
                                  PushBlobResponse *response) = 0;

    virtual grpc::Status PushDirectory(const PushDirectoryRequest &request,
                                       PushDirectoryResponse *response) = 0;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_RAINSTANCE_H
