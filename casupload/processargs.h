#ifndef INCLUDED_CASUPLOAD_PROCESSARGS
#define INCLUDED_CASUPLOAD_PROCESSARGS

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_merklize.h>
#include <cstddef>
#include <map>
#include <string>
#include <vector>

namespace casupload {

struct LocalCasOption {
    bool d_useLocalCas = false;
    bool d_bypassLocalCache = false;
};
struct ProcessedArgs {
    bool d_processed;
    bool d_valid;

    bool d_dryRunMode;
    buildboxcommon::LogLevel d_logLevel;

    buildboxcommon::ConnectionOptions d_casConnectionOptions;

    bool d_followSymlinks;
    bool d_captureMtime;
    bool d_captureUnixMode;
    mode_t d_fileUmask = 0;
    size_t d_numUploadThreads = 0;
    size_t d_numDigestThreads = 0;
    LocalCasOption d_localCasOption{};

    std::vector<std::string> d_paths;
    std::string d_ignoreFile;
    buildboxcommon::Command_OutputDirectoryFormat d_directoryFormat;
    std::string d_outputRootDigestFile;
    std::string d_outputTreeDigestFile;
    std::map<std::string, std::string> d_nodeProperties;

    buildboxcommon::DigestFunction_Value d_digestFunctionValue;
};

ProcessedArgs processArgs(int argc, char *argv[]);

} // namespace casupload

#endif
