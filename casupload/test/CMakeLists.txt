include(${CMAKE_SOURCE_DIR}/cmake/BuildboxGTestSetup.cmake)

set(target casupload_test)
add_executable(casupload_test casupload.t.cpp ../processargs.cpp ../processargs.h)
target_precompile_headers(casupload_test REUSE_FROM common)

target_include_directories(casupload_test PRIVATE "..")
target_link_libraries(casupload_test common ${GTEST_MAIN_TARGET} ${GTEST_TARGET})

add_test(NAME casupload_test COMMAND casupload_test WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
