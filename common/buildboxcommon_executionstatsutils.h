/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCOMMON_EXECUTIONSTATSUTILS
#define INCLUDED_BUILDBOXCOMMON_EXECUTIONSTATSUTILS

#include <build/buildbox/execution_stats.grpc.pb.h>

#include <sys/resource.h>

namespace buildboxcommon {

struct ExecutionStatsUtils {
    // Call `rusage(2)` to fetch the usage statistics of child processes.
    static build::buildbox::ExecutionStatistics getChildrenProcessRusage();

    // Return a `ProcessResourceUsage` proto populated with relevant values
    // from `rusage`.
    static build::buildbox::ExecutionStatistics_ProcessResourceUsage
    processResourceUsageFromRusage(const rusage &ru);
};

} // namespace buildboxcommon

#endif
