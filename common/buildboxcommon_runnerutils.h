#include <optional>
#include <string>

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_mergeutil.h>

/**
 * RunnerUtils was created to contain duplicated functions and constants used
 * by some but not all variations of the runner.
 */

namespace buildboxcommon {
namespace buildboxrun {

/**
 * Check Platform properties for the chroot root digest,
 * ensure that's not empty and return 'chrootRootDigest' if available
 */
std::optional<Digest> hasChrootRootDigest(const Platform &platform);

/**
 * Check Platform properties for the chroot tree digest,
 * ensure that's not empty and populate return 'chrootTreeDigest' if available
 */
std::optional<Digest> hasChrootTreeDigest(const Platform &platform);

/**
 * Combines an input tree with a chroot tree into a collection of blobs and
 * digests stored in mergedDirectoryBlobs. Returns the digest of the root
 * directory.
 */
Digest mergeTrees(const MergeUtil::DirectoryTree &inputTree,
                  const MergeUtil::DirectoryTree &chrootTree,
                  digest_string_map *mergedDirectoryBlobs);

} // namespace buildboxrun
} // namespace buildboxcommon