/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_logging.h>

#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iterator>
#include <sstream>

namespace buildboxcommon {

namespace logging {

const std::map<std::string, LogLevel> &stringToLogLevelMap()
{
    static const std::map<std::string, LogLevel> map = {
        {"trace", LogLevel::TRACE},
        {"debug", LogLevel::DEBUG},
        {"info", LogLevel::INFO},
        {"warning", LogLevel::WARNING},
        {"error", LogLevel::ERROR}};

    return map;
}

const std::map<LogLevel, std::string> &logLevelToStringMap()
{
    static const std::map<LogLevel, std::string> map = {
        {LogLevel::TRACE, "trace"},
        {LogLevel::DEBUG, "debug"},
        {LogLevel::INFO, "info"},
        {LogLevel::WARNING, "warning"},
        {LogLevel::ERROR, "error"}};

    return map;
}

Logger::Logger() {}

Logger &Logger::getLoggerInstance()
{
    static Logger logger;
    return logger;
}

void Logger::setOutputDirectory(const char *outputDirectory)
{
    if (d_glogInitialized) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error, "Output directories must be specified "
                                "before Logger instance is initialized.");
    }

    d_logOutputDirectory = outputDirectory ? outputDirectory : "";
}

std::string Logger::getOutputDirectory() { return d_logOutputDirectory; }

void Logger::enableLoggingBothStderrAndFiles()
{
    // Error if logger is already initialized
    if (d_glogInitialized) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "Attempted to enable logging to both stderr and files after "
            "Logger instance was initialized.");
    }
    // Error if output directory is not set
    if (d_logOutputDirectory.empty()) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "Output directory must be set before enabling logging to both "
            "stderr and files.");
    }
    d_logBothStderrAndFiles = true;
}

void Logger::initialize(const char *programName)
{
    if (programName == nullptr || strlen(programName) == 0) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "Initialize() must be called with a non-empty program name");
    }

    if (d_glogInitialized) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "Attempted to initialize Logger instance more than once.");
    }

    if (!d_logOutputDirectory.empty()) {
        FLAGS_log_dir = d_logOutputDirectory;
        FLAGS_logtostderr = false;
        if (d_logBothStderrAndFiles) {
            FLAGS_alsologtostderr = true;
        }
        else {
            FLAGS_alsologtostderr = false;
        }
    }
    else {
        FLAGS_logtostderr = true;
    }

    // Since we will add our own, we also disable glog's own prefix using
    // the following global variable.
    FLAGS_log_prefix = false;

    google::InitGoogleLogging(programName);
    d_glogInitialized = true;
}

namespace {
class LogDisable {
  public:
    /**
     * Provides a way to disable logging temporarily for the lifetime
     * of this object.
     *
     * Used to circumvent google::SetVLOGLevel writing a RAW_VLOG without
     * going through the buildbox log formatters. Non-formatted log lines
     * cause trouble in log consumers which expect uniform log lines.
     *
     * This class is not threadsafe. If other threads are actively logging,
     * this method will interrupt any logs being posted from those threads.
     * Use with caution.
     */
    explicit LogDisable()
        : d_orig_FLAGS_logtostderr(FLAGS_logtostderr),
          d_orig_FLAGS_alsologtostderr(FLAGS_alsologtostderr),
          d_orig_FLAGS_log_dir(FLAGS_log_dir)
    {
        FLAGS_logtostderr = false;
        FLAGS_alsologtostderr = false;
        FLAGS_log_dir.clear();
    }

    ~LogDisable()
    {
        FLAGS_logtostderr = d_orig_FLAGS_logtostderr;
        FLAGS_alsologtostderr = d_orig_FLAGS_alsologtostderr;
        FLAGS_log_dir = d_orig_FLAGS_log_dir;
    }

    // Disable copy and move.
    LogDisable(const LogDisable &) = delete;
    LogDisable &operator=(const LogDisable &) = delete;

    LogDisable(LogDisable &&) = delete;
    LogDisable &operator=(LogDisable &&) = delete;

  private:
    bool d_orig_FLAGS_logtostderr;
    bool d_orig_FLAGS_alsologtostderr;
    std::string d_orig_FLAGS_log_dir;
};
} // namespace

void Logger::setLogLevel(LogLevel logLevel)
{
    /* By default show all messages */
    google::LogSeverity glogMinSeverity = google::GLOG_INFO;
    if (logLevel == buildboxcommon::LogLevel::WARNING) {
        glogMinSeverity = google::GLOG_WARNING;
    }
    else if (logLevel == buildboxcommon::LogLevel::ERROR) {
        glogMinSeverity = google::GLOG_ERROR;
    }

    FLAGS_minloglevel = glogMinSeverity;

    /* For the verbose levels, we also set the VLOG max. level */
    if (logLevel == LogLevel::DEBUG) {
        LogDisable disable;
        google::SetVLOGLevel("*", buildboxcommon::GLOG_VLOG_LEVEL_DEBUG);
    }
    else if (logLevel == LogLevel::TRACE) {
        LogDisable disable;
        google::SetVLOGLevel("*", buildboxcommon::GLOG_VLOG_LEVEL_TRACE);
    }

    d_logLevel = logLevel;
}

LogLevel Logger::getLogLevel() { return d_logLevel; }

void Logger::disableStderr()
{
    FLAGS_logtostderr = false;
    FLAGS_alsologtostderr = false;

    // Setting the minimum severity to FATAL, which we never use, to
    // effectively hide all messages from stderr because otherwise glog still
    // prints ERRORs.
    FLAGS_stderrthreshold = google::GLOG_FATAL;
}

std::string stringifyLogLevels()
{
    std::string logLevels;
    for (const auto &stringLevelPair :
         buildboxcommon::logging::logLevelToStringMap()) {
        logLevels += stringLevelPair.second + "/";
    }
    logLevels.pop_back();
    return logLevels;
}

std::string printableCommandLine(const std::vector<std::string> &commandLine)
{
    if (commandLine.empty()) {
        return "";
    }
    std::ostringstream commandLineStream;
    // -1, to avoid putting space at end of string
    copy(commandLine.begin(), commandLine.end() - 1,
         std::ostream_iterator<std::string>(commandLineStream, " "));
    commandLineStream << commandLine.back();
    return commandLineStream.str();
}

std::string logPrefix(const std::string &severity, const std::string &file,
                      const int lineNumber)
{
    std::ostringstream os;
    writeLogPrefix(severity, file, lineNumber, os);
    return os.str();
}

std::ostream &writeLogPrefix(const std::string &severity,
                             const std::string &file, const int lineNumber,
                             std::ostream &os)
{
    const std::chrono::system_clock::time_point now =
        std::chrono::system_clock::now();
    const time_t nowAsTimeT = std::chrono::system_clock::to_time_t(now);
    const std::chrono::milliseconds nowMs =
        std::chrono::duration_cast<std::chrono::milliseconds>(
            now.time_since_epoch()) %
        1000;
    struct tm localtime {};
    localtime_r(&nowAsTimeT, &localtime);

    const auto basenameStart = std::max<size_t>(file.find_last_of("/") + 1, 0);

    os << std::put_time(&localtime, "%FT%T") << '.' << std::setfill('0')
       << std::setw(3) << nowMs.count() << std::put_time(&localtime, "%z")
       << " [" << getpid() << ":" << pthread_self() << "] ["
       << file.substr(basenameStart) << ":" << lineNumber << "] ";
    os << "[" << severity << "] ";

    return os;
}

} // namespace logging

} // namespace buildboxcommon
