# logstreamreceiver

This listens for LogStream API `ByteStream.Write()` requests and echoes the
data it receives to stdout.

## Usage

```
Usage: ./logstreamreceiver
   --help     Display usage and exit [optional]
   --bind     Address or socket to bind to [optional, default = "localhost:50070"]
```

It will listen for requests until it is signaled to exit.


This utility can be used to receive data sent by the `outputstreamer` tool in
this repo.

## `QueryWriteStatus()`
Currently `logstreamreceiver` only allows appending data at the end and does not support resuming.

However, it implements a dummy servicer for `ByteStream.QueryWriteStatus()` for clients
that need it in order to detect whether the stream is being consumed by at least a reader before they start writing to it.

It returns a `QueryWriteStatusResponse` `r` such that:
  * `r.committed_size == 0`
  * `r.complete == false`
