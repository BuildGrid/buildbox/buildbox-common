# defaults
prefix := /usr/local
exec_prefix := $(prefix)
bindir := $(exec_prefix)/bin
libexecdir := $(exec_prefix)/libexec
sysconfdir := $(prefix)/etc
srcdir := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

CC := /usr/bin/cc
CXX := /usr/bin/c++
RECC := $(libexecdir)/recc
RECC_CONFIG_PREFIX := $(sysconfdir)/etc

# platform properties
RECC_REMOTE_PLATFORM_ISA := $(shell uname -m)
RECC_REMOTE_PLATFORM_OSFamily := $(shell uname -s)
RECC_REMOTE_PLATFORM_OSRelease := $(shell uname -r)
 
# targets
RECC_CC := recc-cc
RECC_CXX := recc-c++
RECC_CONF := recc.conf

# templates
RECC_WRAPPER_TEMPLATE = $(srcdir)/recc-wrapper.in
RECC_CONF_TEMPLATE = $(srcdir)/recc.conf.in

.DEFAULT_GOAL := all

.PHONY: all
all: $(RECC_CONF) $(RECC_CC) $(RECC_CXX)
	
.PHONY: clean
clean:
	@rm -f $(RECC_CONF) $(RECC_CC) $(RECC_CXX)

# envsubst does not allow excluding a variable from substitution.
# Use RECC_CONFIG_DIRECTORY=$$${RECC_CONFIG_DIRECTORY} as a workaround
# Use $$ to tell make not to expand the variable
$(RECC_CONF): $(RECC_CONF_TEMPLATE) 
	@echo "Creating $@..."
	@envsubst < $(RECC_CONF_TEMPLATE) > $@
	RECC_CONFIG_DIRECTORY='$${RECC_CONFIG_DIRECTORY}' \
		RECC_SERVER=$(RECC_SERVER) \
		RECC_INSTANCE=$(RECC_INSTANCE) \
		RECC_CONFIG_PREFIX=$(RECC_CONFIG_PREFIX) \
		RECC_REMOTE_PLATFORM_ISA=$(RECC_REMOTE_PLATFORM_ISA) \
		RECC_REMOTE_PLATFORM_OSFamily=$(RECC_REMOTE_PLATFORM_OSFamily) \
		RECC_REMOTE_PLATFORM_OSRelease=$(RECC_REMOTE_PLATFORM_OSRelease) \
		envsubst < $(RECC_CONF_TEMPLATE) > $@

# macro to apply wrapper template
define APPLY_WRAPPER_TEMPLATE
@echo "Creating $(1) ..."
@install -d $(dir $(1))
@RECC=$(RECC) COMMAND=$(2) envsubst < $(RECC_WRAPPER_TEMPLATE) > $(1)
@chmod +x $(1)
endef

$(RECC_CC): $(RECC_WRAPPER_TEMPLATE)
	$(call APPLY_WRAPPER_TEMPLATE,$@,$(CC))

$(RECC_CXX): $(RECC_WRAPPER_TEMPLATE)
	$(call APPLY_WRAPPER_TEMPLATE,$@,$(CXX))
