# casdownload

`casdownload` facilitates downloading blobs, action outputs and directory trees
from a previously configured Content Addressable Store and Action Cache (such
as BuildGrid, Buildbarn or Buildfarm).

## Usage

```
Usage: ./casdownload
   --help                      Display usage and exit.
   --version                   Print version information and exit. [optional]
   --remote                    URL for the CAS service [optional]
   --instance                  Name of the CAS instance [optional]
   --server-cert               Server TLS certificate for CAS (PEM-encoded) [optional]
   --client-key                Client private TLS key for CAS (PEM-encoded) [optional]
   --client-cert               Client TLS certificate for CAS (PEM-encoded) [optional]
   --access-token              Authentication token for CAS (JWT, OAuth token etc), will be included as an HTTP  Authorization bearer token [optional]
   --token-reload-interval     Access token refresh timeout for CAS service [optional]
   --googleapi-auth            Use GoogleAPIAuth for CAS service [optional]
   --retry-limit               Retry limit for gRPC errors for CAS service [optional]
   --retry-delay               Retry delay for gRPC errors for CAS service [optional]
   --retry-on-code             gRPC status code(s) as string(s) to retry on for CAS service e.g., 'UNKNOWN', 'INTERNAL' [optional]
   --request-timeout           Timeout for gRPC requests for CAS service (set to 0 to disable timeout) [optional]
   --min-throughput            Minimum throughput for gRPC requests for CAS service, bytes per seconds. The value may be suffixed with K, M, G or T. [optional]
   --keepalive-time            gRPC keepalive pings period for CAS service (set to 0 to disable keepalive pings) [optional]
   --load-balancing-policy     gRPC load balancing policy for CAS service (valid options are 'round_robin' and 'grpclb') [optional]
   --destination-dir           Directory to save downloaded data [required]
   --root-digest               Download a tree by its root ID [optional]
   --tree-digest               Download a tree from a single blob containing a tree message [optional]
   --action-digest             Download the stderr, stdout, output files and directories of this digest via the ActionCache [optional]
   --file-digest               Download a blob by its content [optional]
   --cas-server                [Deprecated] Use --remote [optional]
   --digest-function           Set a custom digest function. Default: SHA256
                                  Supported functions: SHA384, SHA512, SHA256, SHA1, MD5 [optional]
   --log-level                 Log level (debug, error, info, trace, warning) [optional, default = "error"]
   --verbose                   Set log level to 'debug' [optional]

Download the specified digest(root/tree/action/file) into the specified directory

Action digest download expects that ActionCache is hosted on the same
server as the CAS server.

Example usage:
  casdownload --remote=http://localhost:50051
              --destination-dir=/path/to/output
              --root-digest=deafbeef/123
```

### Examples with Docker

`casdownload` can be used via the [`Dockerfile`](Dockerfile). The following
illustrates the `file-digest` mode to download a single blob from the CAS:

```
$ cd cpp/casdownload
$ docker build . -t casdownload
...
$ docker run -it casdownload bash
$ casdownload --instance=dev --remote=http://my-cas-server:60051 --file-digest=9fca6f98a3b46af41101fca85842fc52eb0cee16abb92dc4be6aee87afd4994d/142 --destination-dir=dir
CAS client connecting to http://my-cas-server:60051
Starting to download 9fca6f98a3b46af41101fca85842fc52eb0cee16abb92dc4be6aee87afd4994d/142 to "dir"
Downloaded blob to dir/blob
Finished downloading 9fca6f98a3b46af41101fca85842fc52eb0cee16abb92dc4be6aee87afd4994d/142 to "dir" in 0.036 second(s)
$ cat dir/blob
Hello, world!
$
```

To download an Action:

```
$ casdownload --instance=dev --remote=http://my-cas-server:40051 --action-digest=0125ae312e4743af312bc6107558ecb1d4dc79eeb8c2314285c28f825ccede5e/142 --destination-dir=actionresult
CAS client connecting to http://my-cas-server:40051
Starting to download 0125ae312e4743af312bc6107558ecb1d4dc79eeb8c2314285c28f825ccede5e/142 to "actionresult"
Finished downloading 0125ae312e4743af312bc6107558ecb1d4dc79eeb8c2314285c28f825ccede5e/142 to "actionresult" in 1.485 second(s)
$ cat actionresult/stdout
Hello, world stdout!
$ cat actionresult/stderr
Hello, world stderr!
$ ls actionresult
myoutputfile
stderr
stdout
$
```
