// Copyright 2024 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <buildboxcommon_logging.h>
#include <env.h>
#include <fileutils.h>
#include <regex>

namespace recc {

bool checkAnyInputPathMatchesRegex(
    const std::string regexString,
    const std::vector<std::string> &stringsToMatch,
    const std::string &currentWorkingDirectory)
{
    std::regex regexToMatch;
    try {
        regexToMatch = std::regex(regexString);
    }
    catch (const std::regex_error &e) {
        BUILDBOX_LOG_ERROR("Invalid regex pattern: " << regexString);
        return false;
    }
    for (auto stringToMatch : stringsToMatch) {
        if (!FileUtils::isAbsolutePath(stringToMatch)) {
            stringToMatch = currentWorkingDirectory + "/" + stringToMatch;
        }
        if (std::regex_match(stringToMatch, regexToMatch)) {
            BUILDBOX_LOG_DEBUG("Input String: " << stringToMatch
                                                << " matches regex: "
                                                << regexString)
            return true;
        }
    }
    return false;
}

bool matchIgnorePaths(const std::set<std::string> regexPaths,
                      const std::vector<std::string> &inputFiles,
                      const std::string &currentWorkingDirectory)
{
    for (const auto &regexPath : regexPaths) {
        if (checkAnyInputPathMatchesRegex(regexPath, inputFiles,
                                          currentWorkingDirectory)) {
            return true;
        }
    }
    return false;
}
} // namespace recc