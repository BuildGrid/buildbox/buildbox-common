// Copyright 2024 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <regexutils.h>

#include <string>
#include <vector>

#include <gtest/gtest.h>

static const std::string TIMER_NAME_CALCULATE_DIGESTS_TOTAL =
    "recc.calculate_digests_total";

using namespace recc;

TEST(RegexUtilsTest, TestMatchIgnorePaths)
{
    const std::set<std::string> regexPaths({".*/hello/CmakeScratch/.*",
                                            ".*/TryCompile/.*",
                                            "/ignoreDir[0-9]+/.*"});
    std::string cwd = "/ignoreDir2";
    ASSERT_TRUE(matchIgnorePaths(
        regexPaths, std::vector<std::string>({"file.cpp", "hello.cpp"}), cwd));
    cwd = "/hello/TryCompile";
    ASSERT_TRUE(matchIgnorePaths(
        regexPaths, std::vector<std::string>({"file.cpp", "hello.cpp"}), cwd));
    cwd = "/home/user/dir";
    ASSERT_TRUE(matchIgnorePaths(
        regexPaths,
        std::vector<std::string>(
            {"/lorem/TryCompiled/", "./hello/CmakeScratch/xyz"}),
        cwd));
    ASSERT_TRUE(matchIgnorePaths(
        regexPaths,
        std::vector<std::string>({"file.cpp", "./build/TryCompile/xyz"}),
        cwd));
    ASSERT_TRUE(matchIgnorePaths(
        regexPaths, std::vector<std::string>({"/ignoreDir21778298/filea"}),
        cwd));
    ASSERT_TRUE(matchIgnorePaths(
        regexPaths,
        std::vector<std::string>({"file.cpp", "/ignoreDir/", "/ignoreDir0/"}),
        cwd));

    ASSERT_FALSE(matchIgnorePaths(
        regexPaths, std::vector<std::string>({"/lorem/TryCompiled/"}), cwd));
    ASSERT_FALSE(matchIgnorePaths(
        regexPaths,
        std::vector<std::string>({"file.cpp", "./build/CmakeScratch/xyz"}),
        cwd));
    ASSERT_FALSE(matchIgnorePaths(
        regexPaths, std::vector<std::string>({"file.cpp", "/ignoreDir/"}),
        cwd));
}