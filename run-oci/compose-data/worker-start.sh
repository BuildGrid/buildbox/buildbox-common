#!/bin/sh

BUILDBOX_E2E_TEMPORARY_DIRECTORY=`sudo -u casuser mktemp -d`
BUILDBOX_CASD_LOCAL_CACHE=$BUILDBOX_E2E_TEMPORARY_DIRECTORY/casd

# Sleep to give buildgrid time to startup
sleep 1

# Launch Casd as casuser
sudo -u casuser $CASD_BINARY \
--verbose \
--cas-remote=${BUILDGRID_SERVER_URL} \
--bind=$BIND_LOCATION ${BUILDBOX_CASD_LOCAL_CACHE} &

# Launch Worker/Runner as runuser
sudo -u runuser $WORKER_BINARY \
--verbose \
--buildbox-run=$RUNNER_BINARY \
--runner-arg=--memory-limit=2000000000 \
--runner-arg=--cpu-time=120 \
--platform chrootRootDigest=${CHROOT_DIGEST} \
--bots-remote=${BUILDGRID_SERVER_URL} \
--cas-remote=http://$BIND_LOCATION
