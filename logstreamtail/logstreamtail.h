/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_LOGSTREAMTAIL_H
#define INCLUDED_LOGSTREAMTAIL_H

#include <buildboxcommon_connectionoptions.h>

#include <google/bytestream/bytestream.grpc.pb.h>

#include <functional>
#include <string>

struct LogStreamTail {

    typedef std::function<void(const std::string &)> DataAvailableCallback;

    static grpc::Status
    readLogStream(const buildboxcommon::ConnectionOptions &connectionOptions,
                  const std::string &resourceName,
                  const DataAvailableCallback &dataAvailableCallback);

    static grpc::Status

    readLogStream(
        const std::shared_ptr<google::bytestream::ByteStream::StubInterface>
            &byteStreamStub,
        const std::string &resourceName,
        const DataAvailableCallback &dataAvailableCallback);
};

#endif
