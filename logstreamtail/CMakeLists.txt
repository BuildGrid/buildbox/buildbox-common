add_executable(logstreamtail logstreamtail.m.cpp logstreamtail.cpp)
target_precompile_headers(logstreamtail REUSE_FROM common)
target_include_directories(logstreamtail PRIVATE ".")
target_link_libraries(logstreamtail common)
install(TARGETS logstreamtail RUNTIME DESTINATION bin COMPONENT logstreamtail)

include(CTest)
if(BUILD_TESTING)
    add_subdirectory(test)
endif()
